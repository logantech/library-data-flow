# Information system modeling - Library book recommendation system

Build a data flow diagram representing a system to recommend the "next book" for library patrons based on their browsing habits and book recommendations.

* Used Lucidchart to create a custom data flow diagram
* Includes data stores, data arrows and terminators, and process flows

Quickstart:

* Download the PDF of the
[data flow diagram](Library-Patron-Book-Recommendation-System.pdf)

___


![Data flow diagram of a system to recommend library books](Library-Patron-Book-Recommendation-System.png)
